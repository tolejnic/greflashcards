from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
from  sqlalchemy.sql.expression import func
import json
import os
from random import randint
import requests


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://jlrhsequybnylo:6ff369ae91e0621e00c3b017ad3d81a9dfde665efc5e8fbaeb82a8329a21c678@ec2-54-243-128-95.compute-1.amazonaws.com:5432/deq59sdpfj5utn'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Flashcard(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(80))
    definition = db.Column(db.Text)
    sentence = db.Column(db.Text)
    def __init__(self, word, definition, sentence):
        self.word = word
        self.definition = definition
        self.sentence = sentence


@app.route('/')
def flashcards():
	rowId = Flashcard.query.order_by(func.random()).first().id
	flashcard = Flashcard.query.get(rowId)
	return render_template("index.html", flashcard=flashcard)


@app.route('/search/<string:word>/')
def search(word):
	if word == "":
		definition = ""
	else:
		definition = find_definition(word)
	return render_template("index.html", word=word, definition=definition)


@app.route("/flashcards/create", methods=["GET", "POST"])
def create_flashcard():
	if request.method == "GET":
		return render_template("create_flashcard.html")
	else:
		word = request.form["word"]
		definition, sentence = find_definition(word)
		flashcard = Flashcard(word=word, definition=definition, sentence=sentence)
		db.session.add(flashcard)
		db.session.commit()
		return redirect("/flashcards")


@app.route("/flashcards", methods=["GET"])
def list_flashcards():
		flashcards = Flashcard.query.order_by(Flashcard.id).all()
		return render_template("list_flashcards.html", flashcards=flashcards)


@app.route("/flashcards/edit/<string:pid>", methods=["GET", "POST"])
def edit_flashcards(pid):
	flashcard = Flashcard.query.get(int(pid))
	if request.method == "GET":
		return render_template("edit_flashcard.html", flashcard=flashcard)
	else:
		flashcard.word = request.form["word"]
		flashcard.definition = request.form["definition"]
		flashcard.sentence = request.form["sentence"]
		db.session.commit()
		return redirect("/flashcards")


@app.route("/flashcards/delete/<string:pid>", methods=["GET"])
def delete_flashcards(pid):
	flashcard = Flashcard.query.get(int(pid))
	db.session.delete(flashcard)
	db.session.commit()
	return redirect("/flashcards")


'''
searches dictionary for keys and returns all values
'''
def find_values(id, json_repr):
	results = []

	def _decode_dict(a_dict):
		try: results.append(a_dict[id])
		except KeyError: pass
		return a_dict

	json.loads(json_repr, object_hook=_decode_dict)  # Return value ignored.
	return results[0:2]


def find_definition(word):
	r = requests.get("https://www.dictionaryapi.com/api/v3/references/collegiate/json/" + word + "?key=fb1800b4-0ec0-4f7f-83b4-2139aa9f6507")
	json_data = json.loads(r.text)
	definition = ','.join(json_data[0]['shortdef'])
	values = find_values('t', r.text)
	sentence = '<br />'.join(values).replace("{wi}", "<b>").replace("{/wi}", "</b>").replace("{qword}", "<b>").replace("{/qword}", "</b>").replace("{it}", "<b>").replace("{/it}", "</b>")
	return definition, sentence


@app.route("/update_from_file", methods=["GET"])
def update_from_file():
	try:
		flashcards = Flashcard.query.order_by(Flashcard.id).all()
		for flashcard in flashcards:
			db.session.delete(flashcard)
			db.session.commit()
	except:
		pass

	with open("/Users/tim/web_dev/flashcards/static/words.txt") as data:
		words = data.read().split('\n')

	for word in words:
		definition, sentence = find_definition(word)
		w = Flashcard(word, definition, sentence)
		db.session.add(w)
		db.session.commit()
	return redirect("/flashcards")


if __name__ == "__main__":
	app.run(debug=True)